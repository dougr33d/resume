DEPS = cv-sections/committees.tex cv-sections/education.tex cv-sections/experience.tex \
       cv-sections/extracurricular.tex cv-sections/honors.tex cv-sections/presentation.tex \
       cv-sections/skills.tex cv-sections/writing.tex cv-sections/patents.tex awesome-cv.cls cv-sections/summary.tex

.PHONY: build-resume
build-resume: build-image
	docker run -it --rm --name=resume \
		--mount type=bind,source=${PWD},target=/src \
		dougr33d/resume_build:0.1 

.PHONY: build-image
build-image:
	docker build -t dougr33d/resume_build:0.1 .

resume.pdf: resume.tex ${DEPS}
	xelatex $<

