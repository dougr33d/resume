FROM ubuntu:18.04
LABEL Description="Resume build environment"

ENV HOME /root

SHELL ["/bin/bash", "-c"]

RUN apt-get update
RUN apt-get -y --no-install-recommends install \
        build-essential \
        texlive \
        texlive-fonts-extra \
        texlive-latex-extra \
        texlive-xetex 

WORKDIR /src

CMD make resume.pdf

